package itcsolutions.pratice.example;

/**
 * Основной класс для практики. В нем содержится основная точка входа - метод main. Именно он запускается при старте приложения
 *
 * @author mikhailyuk
 * @since 29.06.2021
 */
public class PracticeMainClass {

    /**
     * Входная точка программы
     * @param args - стандратная переменная для этого метода (передача данных при старте приложения, в примере не используем)
     */
    public static void main(String [] args) {
        //Инстанцируем новый объект
        ObjectExample varWithLinkToNewObject = new ObjectExample();
        //Получаем значение переменной из объекта
        String exampleStringValue = varWithLinkToNewObject.getExampleStringValue();
        //Создадим новую переменную для вывода в консоль на основе полученной
        String newStringValue = exampleStringValue + "-utyatki";
        //Вызовем метод объекта, который выводит значение в консоль и передадим в него новую переменную
        varWithLinkToNewObject.exampleMethod2(newStringValue);
    }
}
